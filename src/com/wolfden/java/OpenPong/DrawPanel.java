package com.wolfden.java.OpenPong;

import javax.swing.*;
import java.awt.*;

public class DrawPanel extends JPanel implements Renderable {
    Rectangle bounds;
    Ball mBall;
    public Paddle playerPaddle;

    public DrawPanel() {
        bounds = new Rectangle(Pong.WIDTH,Pong.HEIGHT);
        this.setDoubleBuffered(true);
        this.setBackground(Color.BLACK);
        this.setOpaque(true);

        playerPaddle = new Paddle(250, 600);
        mBall = new Ball.Builder().with(this).createBall();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        render(g);
    }

    public void render(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setColor(Color.white);
        mBall.draw(g2d);
        playerPaddle.draw(g2d);
    }

    @Override
    public void onRender() {
        repaint();
    }

    @Override
    public void onUpdate() {
        mBall.move();
        playerPaddle.move();
    }

    public Rectangle getBounds(){
        return new Rectangle(Pong.WIDTH, Pong.HEIGHT);
    }
}
