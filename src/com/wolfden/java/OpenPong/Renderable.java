package com.wolfden.java.OpenPong;

/**
 * Created by Nish on 12/15/14.
 */
public interface Renderable {
    public void onRender();
    public void onUpdate();
}
