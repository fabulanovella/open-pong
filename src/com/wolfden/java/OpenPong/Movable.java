package com.wolfden.java.OpenPong;

/**
 * Created by Nish on 1/1/15.
 */
public interface Movable {
    public void move();
}
