package com.wolfden.java.OpenPong;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Pong extends JFrame implements KeyListener {
    public static final int WIDTH = 500;
    public static final int HEIGHT = 650;

    DrawPanel panel;
    private boolean running = true;
    private int UPDATES_PER_SECOND = 60;
    private int FRAMES_PER_SECOND = 60;

    private boolean isKeyLeft = false;
    private boolean isKeyRight = false;


    public Pong() {
        init();
        Thread gameLoop = new Thread(new Runnable() {
            @Override
            public void run() {
                startGame();
            }
        });

        gameLoop.start();
        this.addKeyListener(this);
    }

    private void startGame() {

        long initialTime = System.nanoTime();
        final double timeU = 1000000000 / UPDATES_PER_SECOND;
        final double timeF = 1000000000 / FRAMES_PER_SECOND;
        double deltaU = 0, deltaF = 0;
        int frames = 0, ticks = 0;
        long timer = System.currentTimeMillis();

        while (running) {

            long currentTime = System.nanoTime();
            deltaU += (currentTime - initialTime) / timeU;
            deltaF += (currentTime - initialTime) / timeF;
            initialTime = currentTime;

            if (deltaU >= 1) {
                getInput();
                panel.onUpdate();
                // update();
                ticks++;
                deltaU--;
            }

            if (deltaF >= 1) {
                panel.onRender();
                frames++;
                deltaF--;
            }

            if (System.currentTimeMillis() - timer > 1000) {
                //if (RENDER_TIME) {
                System.out.println(String.format("UPS: %s, FPS: %s", ticks, frames));
                //}
                frames = 0;
                ticks = 0;
                timer += 1000;
            }
        }
    }

    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Pong();
            }
        });

    }

    private void init() {
        setTitle("Open Pong");
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        panel = new DrawPanel();
        getContentPane().add(panel);
        setResizable(false);
        setVisible(true);
    }

    private void getInput() {
        if (isKeyRight) {
            panel.playerPaddle.setDx(2);
        } else if (isKeyLeft) {
            panel.playerPaddle.setDx(-2);
        } else {
            panel.playerPaddle.setDx(0);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                isKeyLeft = true;
                break;
            case KeyEvent.VK_RIGHT:
                isKeyRight = true;
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                isKeyLeft = false;
                break;
            case KeyEvent.VK_RIGHT:
                isKeyRight = false;
                break;
        }
    }
}
