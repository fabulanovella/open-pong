package com.wolfden.java.OpenPong;

import java.awt.*;

public class Ball extends Entity implements Movable {
    private DrawPanel panel;

    public Ball() {
        height = 10;
        width = 10;

        setX(245);
        setY(245);
        setDx(1);
        setDy(1);
    }

    public Ball(int x, int y, DrawPanel panel) {
        this();

        this.x = x;
        this.y = y;
        this.panel = panel;
    }

    public void draw(Graphics2D g2d) {
        g2d.drawRect(this.getX(), this.getY(), width, height);
    }

    @Override
    public void move() {
        if (!this.getBounds().intersects(panel.getBounds())) {
            dx = -dx;
            dy = -dy;
        }

        x = x + dx;
        y = y + dy;
    }


    static class Builder {
        int x = 250;
        int y = 300;

        DrawPanel context;

        public Builder x(int x) {
            this.x = x;
            return this;
        }

        public Builder y(int y) {
            this.y = y;
            return this;
        }

        public Builder with(DrawPanel panel) {
            this.context = panel;
            return this;
        }

        public Ball createBall() {
            System.out.println("Ball" + x);
            return new Ball(x, y, context);
        }
    }
}
