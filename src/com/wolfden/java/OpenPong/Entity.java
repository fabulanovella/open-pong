package com.wolfden.java.OpenPong;

import java.awt.*;

public abstract class Entity {

    public int x;
    public int y;

    public int height;
    public int width;

    public int dx;
    public int dy;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }


    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getDx() {
        return dx;
    }

    public void setDx(int dx) {
        this.dx = dx;
    }

    public int getDy() {
        return dy;
    }

    public void setDy(int dy) {
        this.dy = dy;
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, width, height);
    }

    public int getTop() {
        return y;
    }

    public int getBottom() {
        return y + height;
    }

    public int getLeft() {
        return x;
    }

    public int getRight() {
        return x + width;
    }

}
