package com.wolfden.java.OpenPong;

import java.awt.*;

public class Paddle extends Entity implements Movable{


	public Paddle(int x, int y) {
		setWidth(100);
		setHeight(10);
		setY(y);
		setX(x);
	}


	public void draw(Graphics2D g2d){
		g2d.drawRect(this.getX(), this.getY(), width, height);
	}


	@Override
	public void move(){
		x = x + dx;
	}
}
